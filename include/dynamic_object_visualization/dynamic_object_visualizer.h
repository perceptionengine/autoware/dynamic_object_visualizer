/*
 * Copyright 2020 Tier IV, Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include <string>
#include <ros/ros.h>
#include <yaml-cpp/yaml.h>
#include "autoware_perception_msgs/DynamicObjectArray.h"
#include "autoware_perception_msgs/DynamicObjectWithFeatureArray.h"
#include "autoware_perception_msgs/PredictedPath.h"
#include "autoware_perception_msgs/Shape.h"
#include "geometry_msgs/Pose.h"
#include "std_msgs/ColorRGBA.h"
#include <geometry_msgs/Point.h>
#include <unique_id/unique_id.h>
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>


class DynamicObjectVisualizer
{
private:  // ros
  ros::NodeHandle nh_;
  ros::NodeHandle private_nh_;
  ros::Publisher pub_;
  ros::Subscriber sub_;

  std_msgs::ColorRGBA box_color_;

  double line_width_;
  double duration_;
  bool draw_centroid_;
  bool draw_coords_;
  bool draw_visibility_markers_;

  static std_msgs::ColorRGBA ParseColor(const std::string &in_color);

  void dynamicObjectWithFeatureCallback(
    const autoware_perception_msgs::DynamicObjectWithFeatureArray::ConstPtr & input_msg);
  void dynamicObjectCallback(
    const autoware_perception_msgs::DynamicObjectArray::ConstPtr & input_msg);
  bool calcBoundingBoxLineList(
    const autoware_perception_msgs::Shape & shape, std::vector<geometry_msgs::Point> & points);
  bool calcCylinderLineList(
    const autoware_perception_msgs::Shape & shape, std::vector<geometry_msgs::Point> & points);
  bool calcCircleLineList(
    const geometry_msgs::Point center, const double radius,
    std::vector<geometry_msgs::Point> & points, const int n = 20);
  bool calcPolygonLineList(
    const autoware_perception_msgs::Shape & shape, std::vector<geometry_msgs::Point> & points);
  bool calcPathLineList(
    const autoware_perception_msgs::PredictedPath & path,
    std::vector<geometry_msgs::Point> & points);
  bool getLabel(const autoware_perception_msgs::Semantic & semantic, std::string & label);
  void getColor(
    const autoware_perception_msgs::DynamicObject & object, std_msgs::ColorRGBA & color);
  void initColorList(std::vector<std_msgs::ColorRGBA> & colors);
  void initPose(geometry_msgs::Pose & pose);

  static float CheckColor(double value);
  static float CheckAlpha(double value);

  visualization_msgs::Marker GetDistanceLabel(double in_distance,
                                              const std_msgs::Header& in_header,
                                              int id);
  visualization_msgs::Marker GetDistanceCircle(double in_distance,
                                               const std_msgs::Header& in_header,
                                               int id);
  visualization_msgs::MarkerArray GenerateVisibilityMarkers(const std_msgs::Header& in_header);

  bool only_known_objects_, only_moving_objects_;
  std::vector<std_msgs::ColorRGBA> colors_;

public:
  DynamicObjectVisualizer();
  virtual ~DynamicObjectVisualizer() {}
};
