/*
 * Copyright 2020 Tier IV, Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "dynamic_object_visualization/dynamic_object_visualizer.h"

DynamicObjectVisualizer::DynamicObjectVisualizer() : nh_(""), private_nh_("~")
{
  bool with_feature;
  private_nh_.param<bool>("with_feature", with_feature, true);
  private_nh_.param<bool>("only_known_objects", only_known_objects_, true);
  private_nh_.param<bool>("only_moving_objects", only_moving_objects_, false);
  private_nh_.param<double>("line_width", line_width_, 0.1);
  private_nh_.param<double>("duration", duration_, 0.1);
  private_nh_.param<bool>("draw_coords", draw_coords_, false);
  private_nh_.param<bool>("draw_centroid", draw_centroid_, false);
  private_nh_.param<bool>("publish_visibility_markers", draw_visibility_markers_, false);

  std::string color;
  private_nh_.param("box_color", color, std::string("[51, 128, 204, 0.8]"));

  box_color_ = ParseColor(color);

  if (with_feature)
    sub_ =
      nh_.subscribe("input", 1, &DynamicObjectVisualizer::dynamicObjectWithFeatureCallback, this);
  else
    sub_ = nh_.subscribe("input", 1, &DynamicObjectVisualizer::dynamicObjectCallback, this);
  pub_ = nh_.advertise<visualization_msgs::MarkerArray>("output", 1, true);
  initColorList(colors_);
}

float DynamicObjectVisualizer::CheckColor(double value)
{
  float final_value;
  if (value > 255.)
    final_value = 1.f;
  else if (value < 0)
    final_value = 0.f;
  else
    final_value = value/255.f;
  return final_value;
}

float DynamicObjectVisualizer::CheckAlpha(double value)
{
  float final_value;
  if (value > 1.)
    final_value = 1.f;
  else if (value < 0.1)
    final_value = 0.1f;
  else
    final_value = value;
  return final_value;
}

std_msgs::ColorRGBA DynamicObjectVisualizer::ParseColor(const std::string &in_color)
{
  ROS_INFO("%s", in_color.c_str());
  YAML::Node color_yml = YAML::Load(in_color);
  float r = 255.,g=255,b=255.,a=1.0;
  if (color_yml.size() != 4)
  {
    ROS_INFO("Color should be four in size");
  }
  else
  {
    r = color_yml[0].as<double>();
    g = color_yml[1].as<double>();
    b = color_yml[2].as<double>();
    a = color_yml[3].as<double>();
  }

  std_msgs::ColorRGBA color;

  if (color_yml.size() == 4) //r,g,b,a
  {
    color.r = CheckColor(r);
    color.g = CheckColor(g);
    color.b = CheckColor(b);
    color.a = CheckAlpha(a);
  }
  return color;
}

visualization_msgs::Marker DynamicObjectVisualizer::GetDistanceLabel(double in_distance,
                                                                        const std_msgs::Header& in_header,
                                                                        int id)
{
  visualization_msgs::Marker text_marker;
  text_marker.lifetime = ros::Duration(duration_);
  text_marker.header = in_header;
  text_marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  text_marker.action = visualization_msgs::Marker::ADD;
  text_marker.ns = "/distance_label";
  text_marker.id = id;
  text_marker.scale.z = 5.0;
  text_marker.pose.position.x = in_distance;
  text_marker.pose.position.z = 2.0;
  text_marker.text = std::to_string(int(in_distance)) + "m";

  text_marker.color.r = 1;
  text_marker.color.g = 1;
  text_marker.color.b = 1;
  text_marker.color.a = 1;
  return text_marker;
}

visualization_msgs::Marker DynamicObjectVisualizer::GetDistanceCircle(double in_distance,
                                                                         const std_msgs::Header& in_header,
                                                                         int id)
{
  visualization_msgs::Marker line_strip;
  line_strip.lifetime = ros::Duration(duration_);
  line_strip.header = in_header;
  line_strip.type = visualization_msgs::Marker::LINE_STRIP;
  line_strip.action = visualization_msgs::Marker::ADD;
  line_strip.ns = "/distance_circle";
  line_strip.id = id;
  line_strip.scale.x = 0.5;

  line_strip.color.r = 41/255.f;
  line_strip.color.g = 112/255.f;
  line_strip.color.b = 69/255.f;
  line_strip.color.a = 0.5;

  for (float theta = 0; theta < 2*3.2f; theta +=0.1)
  {
    geometry_msgs::Point tmp_p;

    tmp_p.x=in_distance*cos(theta);
    tmp_p.y=in_distance*sin(theta);
    line_strip.points.push_back(tmp_p);
  }
  return line_strip;
}

visualization_msgs::MarkerArray
DynamicObjectVisualizer::GenerateVisibilityMarkers(const std_msgs::Header& in_header)
{
  visualization_msgs::MarkerArray vis_objects;

  vis_objects.markers.push_back(GetDistanceCircle(25, in_header, 1));
  vis_objects.markers.push_back(GetDistanceCircle(50, in_header, 2));
  vis_objects.markers.push_back(GetDistanceCircle(100, in_header, 3));
  vis_objects.markers.push_back(GetDistanceCircle(150, in_header, 4));
  vis_objects.markers.push_back(GetDistanceCircle(200, in_header, 5));

  vis_objects.markers.push_back(GetDistanceLabel(25., in_header, 6));
  vis_objects.markers.push_back(GetDistanceLabel(50., in_header, 7));
  vis_objects.markers.push_back(GetDistanceLabel(100., in_header, 8));
  vis_objects.markers.push_back(GetDistanceLabel(150., in_header, 9));
  vis_objects.markers.push_back(GetDistanceLabel(200., in_header, 10));

  vis_objects.markers.push_back(GetDistanceLabel(-25., in_header, 11));
  vis_objects.markers.push_back(GetDistanceLabel(-50., in_header, 12));
  vis_objects.markers.push_back(GetDistanceLabel(-100., in_header, 13));
  vis_objects.markers.push_back(GetDistanceLabel(-150., in_header, 14));
  vis_objects.markers.push_back(GetDistanceLabel(-200., in_header, 15));
  return vis_objects;
}

void DynamicObjectVisualizer::dynamicObjectWithFeatureCallback(
  const autoware_perception_msgs::DynamicObjectWithFeatureArray::ConstPtr & input_msg)
{
  if (pub_.getNumSubscribers() < 1) return;
  boost::shared_ptr<autoware_perception_msgs::DynamicObjectArray> converted_objects_ptr =
    boost::make_shared<autoware_perception_msgs::DynamicObjectArray>();
  converted_objects_ptr->header = input_msg->header;
  for (const auto & feature_object : input_msg->feature_objects) {
    converted_objects_ptr->objects.push_back(feature_object.object);
  }
  dynamicObjectCallback(converted_objects_ptr);
}



void DynamicObjectVisualizer::dynamicObjectCallback(
  const autoware_perception_msgs::DynamicObjectArray::ConstPtr & input_msg)
{
  if (pub_.getNumSubscribers() < 1) return;
  visualization_msgs::MarkerArray output;

  // shape
  for (size_t i = 0; i < input_msg->objects.size(); ++i) {
    if (only_known_objects_) {
      if (input_msg->objects.at(i).semantic.type == autoware_perception_msgs::Semantic::UNKNOWN)
        continue;
    }
    if (only_moving_objects_) {
      if (!input_msg->objects.at(i).state.twist_reliable) {
        continue;
      }
      else {
        double vel = std::sqrt(
          input_msg->objects.at(i).state.twist_covariance.twist.linear.x *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.x +
          input_msg->objects.at(i).state.twist_covariance.twist.linear.y *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.y +
          input_msg->objects.at(i).state.twist_covariance.twist.linear.z *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.z);
        if (vel < 1.0 && input_msg->objects.at(i).semantic.type == autoware_perception_msgs::Semantic::UNKNOWN) {
          continue;
        }
      }
    }
    visualization_msgs::Marker marker;
    marker.header = input_msg->header;
    marker.id = i;
    marker.ns = std::string("shape");
    if (input_msg->objects.at(i).shape.type == autoware_perception_msgs::Shape::BOUNDING_BOX) {
      marker.type = visualization_msgs::Marker::LINE_LIST;
      if (!calcBoundingBoxLineList(input_msg->objects.at(i).shape, marker.points)) continue;
    } else if (input_msg->objects.at(i).shape.type == autoware_perception_msgs::Shape::CYLINDER) {
      marker.type = visualization_msgs::Marker::LINE_LIST;
      if (!calcCylinderLineList(input_msg->objects.at(i).shape, marker.points)) continue;
    } else if (input_msg->objects.at(i).shape.type == autoware_perception_msgs::Shape::POLYGON) {
      marker.type = visualization_msgs::Marker::LINE_LIST;
      if (!calcPolygonLineList(input_msg->objects.at(i).shape, marker.points)) continue;
    } else {
      marker.type = visualization_msgs::Marker::LINE_LIST;
      if (!calcPolygonLineList(input_msg->objects.at(i).shape, marker.points)) continue;
    }

    marker.action = visualization_msgs::Marker::MODIFY;
    marker.pose = input_msg->objects.at(i).state.pose_covariance.pose;
    marker.lifetime = ros::Duration(duration_);
    marker.scale.x = line_width_;
    marker.color = box_color_;

    output.markers.push_back(marker);
  }

  // orientation
  for (size_t i = 0; i < input_msg->objects.size(); ++i) {
    if (!input_msg->objects.at(i).state.orientation_reliable) {
      continue;
    }
    if (only_known_objects_) {
      if (input_msg->objects.at(i).semantic.type == autoware_perception_msgs::Semantic::UNKNOWN)
        continue;
    }
    if (only_moving_objects_) {
      if (!input_msg->objects.at(i).state.twist_reliable) {
        continue;
      }
      else {
        double vel = std::sqrt(
          input_msg->objects.at(i).state.twist_covariance.twist.linear.x *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.x +
          input_msg->objects.at(i).state.twist_covariance.twist.linear.y *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.y +
          input_msg->objects.at(i).state.twist_covariance.twist.linear.z *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.z);
        if (vel < 1.0 && input_msg->objects.at(i).semantic.type == autoware_perception_msgs::Semantic::UNKNOWN) {
          continue;
        }
      }
    }
    visualization_msgs::Marker marker;
    marker.header = input_msg->header;
    marker.id = i;
    marker.type = visualization_msgs::Marker::LINE_LIST;
    marker.ns = std::string("orientation");
    marker.scale.x = line_width_;
    marker.action = visualization_msgs::Marker::MODIFY;
    marker.pose = input_msg->objects.at(i).state.pose_covariance.pose;
    geometry_msgs::Point point;
    point.x = 0.0;
    point.y = 0;
    point.z = (input_msg->objects.at(i).shape.dimensions.z / 2.0);
    marker.points.push_back(point);
    point.x = (input_msg->objects.at(i).shape.dimensions.x / 2.0);
    point.y = 0;
    point.z = (input_msg->objects.at(i).shape.dimensions.z / 2.0);
    marker.points.push_back(point);
    point.x = 0.0;
    point.y = 0;
    point.z = -(input_msg->objects.at(i).shape.dimensions.z / 2.0);
    marker.points.push_back(point);
    point.x = (input_msg->objects.at(i).shape.dimensions.x / 2.0);
    point.y = 0;
    point.z = -(input_msg->objects.at(i).shape.dimensions.z / 2.0);
    marker.points.push_back(point);

    marker.lifetime = ros::Duration(duration_);
    marker.color = box_color_;

    output.markers.push_back(marker);
  }

  // type label
  for (size_t i = 0; i < input_msg->objects.size(); ++i) {
    if (only_known_objects_) {
      if (input_msg->objects.at(i).semantic.type == autoware_perception_msgs::Semantic::UNKNOWN)
        continue;
    }
    if (only_moving_objects_) {
      if (!input_msg->objects.at(i).state.twist_reliable) {
        continue;
      }
      else {
        double vel = std::sqrt(
          input_msg->objects.at(i).state.twist_covariance.twist.linear.x *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.x +
          input_msg->objects.at(i).state.twist_covariance.twist.linear.y *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.y +
          input_msg->objects.at(i).state.twist_covariance.twist.linear.z *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.z);
        if (vel < 1.0 && input_msg->objects.at(i).semantic.type == autoware_perception_msgs::Semantic::UNKNOWN) {
          continue;
        }
      }
    }
    visualization_msgs::Marker marker;
    marker.header = input_msg->header;
    marker.id = i;
    marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
    marker.ns = std::string("label");
    std::string label;
    if (!getLabel(input_msg->objects.at(i).semantic, label)) continue;
    marker.scale.x = 0.5;
    marker.scale.z = 0.5;
    std::string id_str = unique_id::toHexString(input_msg->objects.at(i).id);
    std::remove(id_str.begin(), id_str.end(), '-');
    std::string coords_str;
    if (draw_coords_)
    {
      marker.scale.x = 1.0;
      marker.scale.z = 1.0;
      coords_str = std::to_string(input_msg->objects.at(i).state.pose_covariance.pose.position.x) + 
      "," + std::to_string(input_msg->objects.at(i).state.pose_covariance.pose.position.y) + 
      "," + std::to_string(input_msg->objects.at(i).state.pose_covariance.pose.position.z);
    }
    marker.text = label + ":" + id_str.substr(0, 4) + "\n" + coords_str;
    if (input_msg->objects.at(i).state.twist_reliable) {
      double vel = std::sqrt(
        input_msg->objects.at(i).state.twist_covariance.twist.linear.x *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.x +
        input_msg->objects.at(i).state.twist_covariance.twist.linear.y *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.y +
        input_msg->objects.at(i).state.twist_covariance.twist.linear.z *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.z);
      marker.text = marker.text + "\n" + std::to_string(int(vel * 3.6)) + std::string("[km/h]");
    }
    marker.action = visualization_msgs::Marker::MODIFY;
    marker.pose = input_msg->objects.at(i).state.pose_covariance.pose;
    marker.lifetime = ros::Duration(duration_);
    marker.color.a = 0.999;  // Don't forget to set the alpha!
    marker.color.r = 1.0;
    marker.color.g = 1.0;
    marker.color.b = 1.0;

    output.markers.push_back(marker);
  }

  // twist
  for (size_t i = 0; i < input_msg->objects.size(); ++i) {
    if (!input_msg->objects.at(i).state.twist_reliable) {
      continue;
    }
    if (only_known_objects_) {
      if (input_msg->objects.at(i).semantic.type == autoware_perception_msgs::Semantic::UNKNOWN)
        continue;
    }
    if (only_moving_objects_) {
      if (!input_msg->objects.at(i).state.twist_reliable) {
        continue;
      }
      else {
        double vel = std::sqrt(
          input_msg->objects.at(i).state.twist_covariance.twist.linear.x *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.x +
          input_msg->objects.at(i).state.twist_covariance.twist.linear.y *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.y +
          input_msg->objects.at(i).state.twist_covariance.twist.linear.z *
          input_msg->objects.at(i).state.twist_covariance.twist.linear.z);
        if (vel < 1.0 && input_msg->objects.at(i).semantic.type == autoware_perception_msgs::Semantic::UNKNOWN) {
          continue;
        }
      }
    }
    visualization_msgs::Marker marker;
    marker.header = input_msg->header;
    marker.id = i;
    marker.type = visualization_msgs::Marker::LINE_LIST;
    marker.ns = std::string("twist");
    marker.scale.x = line_width_;
    marker.action = visualization_msgs::Marker::MODIFY;
    marker.pose = input_msg->objects.at(i).state.pose_covariance.pose;
    geometry_msgs::Point point;
    point.x = 0.0;
    point.y = 0;
    point.z = 0;
    marker.points.push_back(point);
    point.x = input_msg->objects.at(i).state.twist_covariance.twist.linear.x;
    point.y = input_msg->objects.at(i).state.twist_covariance.twist.linear.y;
    point.z = input_msg->objects.at(i).state.twist_covariance.twist.linear.z;
    marker.points.push_back(point);

    marker.lifetime = ros::Duration(duration_);
    marker.color.a = 0.999;  // Don't forget to set the alpha!
    marker.color.r = 1.0;
    marker.color.g = 0.0;
    marker.color.b = 0.0;

    output.markers.push_back(marker);
  }

  // path
  {
    int id = 0;
    for (size_t i = 0; i < input_msg->objects.size(); ++i) {
      if (only_known_objects_) {
        if (input_msg->objects.at(i).semantic.type == autoware_perception_msgs::Semantic::UNKNOWN)
          continue;
      }
      if (only_moving_objects_) {
        if (!input_msg->objects.at(i).state.twist_reliable) {
          continue;
        }
        else {
          double vel = std::sqrt(
            input_msg->objects.at(i).state.twist_covariance.twist.linear.x *
            input_msg->objects.at(i).state.twist_covariance.twist.linear.x +
            input_msg->objects.at(i).state.twist_covariance.twist.linear.y *
            input_msg->objects.at(i).state.twist_covariance.twist.linear.y +
            input_msg->objects.at(i).state.twist_covariance.twist.linear.z *
            input_msg->objects.at(i).state.twist_covariance.twist.linear.z);
          if (vel < 1.0 && input_msg->objects.at(i).semantic.type == autoware_perception_msgs::Semantic::UNKNOWN) {
            continue;
          }
        }
      }
      visualization_msgs::Marker marker;
      marker.header = input_msg->header;
      marker.type = visualization_msgs::Marker::LINE_LIST;
      marker.ns = std::string("path");
      marker.action = visualization_msgs::Marker::MODIFY;
      marker.lifetime = ros::Duration(duration_);
      initPose(marker.pose);
      getColor(input_msg->objects.at(i), marker.color);
      for (size_t j = 0; j < input_msg->objects.at(i).state.predicted_paths.size(); ++j) {
        marker.color.a = std::max(
          (double)std::min(
            (double)input_msg->objects.at(i).state.predicted_paths.at(j).confidence, 0.999),
          0.5);
        marker.scale.x = line_width_ * marker.color.a;
        marker.points.clear();
        if (!calcPathLineList(input_msg->objects.at(i).state.predicted_paths.at(j), marker.points))
          continue;
        for (size_t k = 0; k < marker.points.size(); ++k) {
          marker.points.at(k).z -= input_msg->objects.at(i).shape.dimensions.z / 2.0;
        }
        marker.id = ++id;
        output.markers.push_back(marker);
      }
    }
  }

  // path confidence label
  {
    int id = 0;
    for (size_t i = 0; i < input_msg->objects.size(); ++i) {
      if (only_known_objects_) {
        if (input_msg->objects.at(i).semantic.type == autoware_perception_msgs::Semantic::UNKNOWN)
          continue;
      }
      if (only_moving_objects_) {
        if (!input_msg->objects.at(i).state.twist_reliable) {
          continue;
        }
        else {
          double vel = std::sqrt(
            input_msg->objects.at(i).state.twist_covariance.twist.linear.x *
            input_msg->objects.at(i).state.twist_covariance.twist.linear.x +
            input_msg->objects.at(i).state.twist_covariance.twist.linear.y *
            input_msg->objects.at(i).state.twist_covariance.twist.linear.y +
            input_msg->objects.at(i).state.twist_covariance.twist.linear.z *
            input_msg->objects.at(i).state.twist_covariance.twist.linear.z);
          if (vel < 1.0 && input_msg->objects.at(i).semantic.type == autoware_perception_msgs::Semantic::UNKNOWN) {
            continue;
          }
        }
      }
      visualization_msgs::Marker marker;
      marker.header = input_msg->header;
      marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
      marker.ns = std::string("path confidence");
      marker.action = visualization_msgs::Marker::MODIFY;
      marker.lifetime = ros::Duration(duration_);
      marker.scale.x = 0.5;
      marker.scale.y = 0.5;
      marker.scale.z = 0.5;
      initPose(marker.pose);
      getColor(input_msg->objects.at(i), marker.color);
      for (size_t j = 0; j < input_msg->objects.at(i).state.predicted_paths.size(); ++j) {
        if (!input_msg->objects.at(i).state.predicted_paths.at(j).path.empty()) {
          int path_final_index =
            (int)input_msg->objects.at(i).state.predicted_paths.at(j).path.size() - 1;
          marker.pose.position = input_msg->objects.at(i)
                                   .state.predicted_paths.at(j)
                                   .path.at(path_final_index)
                                   .pose.pose.position;
          marker.text =
            std::to_string(input_msg->objects.at(i).state.predicted_paths.at(j).confidence);
          marker.color.a = std::max(
            (double)std::min(
              (double)input_msg->objects.at(i).state.predicted_paths.at(j).confidence, 1.0),
            0.5);
          marker.id = ++id;
          output.markers.push_back(marker);
        }
      }
    }
  }
  if (draw_visibility_markers_)
  {
    visualization_msgs::MarkerArray visibility_markers = GenerateVisibilityMarkers(input_msg->header);

    output.markers.insert(output.markers.end(),
                          visibility_markers.markers.begin(), visibility_markers.markers.end());
  }
  pub_.publish(output);
}

bool DynamicObjectVisualizer::calcBoundingBoxLineList(
  const autoware_perception_msgs::Shape & shape, std::vector<geometry_msgs::Point> & points)
{
  geometry_msgs::Point point;
  point.x = shape.dimensions.x / 2.0;
  point.y = shape.dimensions.y / 2.0;
  point.z = shape.dimensions.z / 2.0;
  points.push_back(point);
  point.x = shape.dimensions.x / 2.0;
  point.y = shape.dimensions.y / 2.0;
  point.z = -shape.dimensions.z / 2.0;
  points.push_back(point);

  point.x = shape.dimensions.x / 2.0;
  point.y = -shape.dimensions.y / 2.0;
  point.z = shape.dimensions.z / 2.0;
  points.push_back(point);
  point.x = shape.dimensions.x / 2.0;
  point.y = -shape.dimensions.y / 2.0;
  point.z = -shape.dimensions.z / 2.0;
  points.push_back(point);

  point.x = -shape.dimensions.x / 2.0;
  point.y = shape.dimensions.y / 2.0;
  point.z = shape.dimensions.z / 2.0;
  points.push_back(point);
  point.x = -shape.dimensions.x / 2.0;
  point.y = shape.dimensions.y / 2.0;
  point.z = -shape.dimensions.z / 2.0;
  points.push_back(point);

  point.x = -shape.dimensions.x / 2.0;
  point.y = -shape.dimensions.y / 2.0;
  point.z = shape.dimensions.z / 2.0;
  points.push_back(point);
  point.x = -shape.dimensions.x / 2.0;
  point.y = -shape.dimensions.y / 2.0;
  point.z = -shape.dimensions.z / 2.0;
  points.push_back(point);

  // up surface
  point.x = shape.dimensions.x / 2.0;
  point.y = shape.dimensions.y / 2.0;
  point.z = shape.dimensions.z / 2.0;
  points.push_back(point);
  point.x = -shape.dimensions.x / 2.0;
  point.y = shape.dimensions.y / 2.0;
  point.z = shape.dimensions.z / 2.0;
  points.push_back(point);

  point.x = shape.dimensions.x / 2.0;
  point.y = shape.dimensions.y / 2.0;
  point.z = shape.dimensions.z / 2.0;
  points.push_back(point);
  point.x = shape.dimensions.x / 2.0;
  point.y = -shape.dimensions.y / 2.0;
  point.z = shape.dimensions.z / 2.0;
  points.push_back(point);

  point.x = -shape.dimensions.x / 2.0;
  point.y = shape.dimensions.y / 2.0;
  point.z = shape.dimensions.z / 2.0;
  points.push_back(point);
  point.x = -shape.dimensions.x / 2.0;
  point.y = -shape.dimensions.y / 2.0;
  point.z = shape.dimensions.z / 2.0;
  points.push_back(point);

  point.x = shape.dimensions.x / 2.0;
  point.y = -shape.dimensions.y / 2.0;
  point.z = shape.dimensions.z / 2.0;
  points.push_back(point);
  point.x = -shape.dimensions.x / 2.0;
  point.y = -shape.dimensions.y / 2.0;
  point.z = shape.dimensions.z / 2.0;
  points.push_back(point);

  // down surface
  point.x = shape.dimensions.x / 2.0;
  point.y = shape.dimensions.y / 2.0;
  point.z = -shape.dimensions.z / 2.0;
  points.push_back(point);
  point.x = -shape.dimensions.x / 2.0;
  point.y = shape.dimensions.y / 2.0;
  point.z = -shape.dimensions.z / 2.0;
  points.push_back(point);

  point.x = shape.dimensions.x / 2.0;
  point.y = shape.dimensions.y / 2.0;
  point.z = -shape.dimensions.z / 2.0;
  points.push_back(point);
  point.x = shape.dimensions.x / 2.0;
  point.y = -shape.dimensions.y / 2.0;
  point.z = -shape.dimensions.z / 2.0;
  points.push_back(point);

  point.x = -shape.dimensions.x / 2.0;
  point.y = shape.dimensions.y / 2.0;
  point.z = -shape.dimensions.z / 2.0;
  points.push_back(point);
  point.x = -shape.dimensions.x / 2.0;
  point.y = -shape.dimensions.y / 2.0;
  point.z = -shape.dimensions.z / 2.0;
  points.push_back(point);

  point.x = shape.dimensions.x / 2.0;
  point.y = -shape.dimensions.y / 2.0;
  point.z = -shape.dimensions.z / 2.0;
  points.push_back(point);
  point.x = -shape.dimensions.x / 2.0;
  point.y = -shape.dimensions.y / 2.0;
  point.z = -shape.dimensions.z / 2.0;
  points.push_back(point);

  return true;
}

bool DynamicObjectVisualizer::calcCylinderLineList(
  const autoware_perception_msgs::Shape & shape, std::vector<geometry_msgs::Point> & points)
{
  int n = 20;
  for (int i = 0; i < n; ++i) {
    geometry_msgs::Point center;
    center.x = 0.0;
    center.y = 0.0;
    center.z = shape.dimensions.z / 2.0;
    calcCircleLineList(center, shape.dimensions.x, points, 20);
  }
  for (int i = 0; i < n; ++i) {
    geometry_msgs::Point center;
    center.x = 0.0;
    center.y = 0.0;
    center.z = -shape.dimensions.z / 2.0;
    calcCircleLineList(center, shape.dimensions.x, points, 20);
  }
  for (int i = 0; i < n; ++i) {
    geometry_msgs::Point point;
    point.x = std::cos(((double)i / (double)n) * 2.0 * M_PI + M_PI / (double)n) *
              (shape.dimensions.x / 2.0);
    point.y = std::sin(((double)i / (double)n) * 2.0 * M_PI + M_PI / (double)n) *
              (shape.dimensions.x / 2.0);
    point.z = shape.dimensions.z / 2.0;
    points.push_back(point);
    point.x = std::cos(((double)i / (double)n) * 2.0 * M_PI + M_PI / (double)n) *
              (shape.dimensions.x / 2.0);
    point.y = std::sin(((double)i / (double)n) * 2.0 * M_PI + M_PI / (double)n) *
              (shape.dimensions.x / 2.0);
    point.z = -shape.dimensions.z / 2.0;
    points.push_back(point);
  }
  return true;
}

bool DynamicObjectVisualizer::calcCircleLineList(
  const geometry_msgs::Point center, const double radius,
  std::vector<geometry_msgs::Point> & points, const int n)
{
  for (int i = 0; i < n; ++i) {
    geometry_msgs::Point point;
    point.x =
      std::cos(((double)i / (double)n) * 2.0 * M_PI + M_PI / (double)n) * (radius / 2.0) + center.x;
    point.y =
      std::sin(((double)i / (double)n) * 2.0 * M_PI + M_PI / (double)n) * (radius / 2.0) + center.y;
    point.z = center.z;
    points.push_back(point);
    point.x =
      std::cos(((double)(i + 1.0) / (double)n) * 2.0 * M_PI + M_PI / (double)n) * (radius / 2.0) +
      center.x;
    point.y =
      std::sin(((double)(i + 1.0) / (double)n) * 2.0 * M_PI + M_PI / (double)n) * (radius / 2.0) +
      center.y;
    point.z = center.z;
    points.push_back(point);
  }
}

bool DynamicObjectVisualizer::calcPolygonLineList(
  const autoware_perception_msgs::Shape & shape, std::vector<geometry_msgs::Point> & points)
{
  if (shape.footprint.points.size() < 2) return false;
  for (size_t i = 0; i < shape.footprint.points.size(); ++i) {
    geometry_msgs::Point point;
    point.x = shape.footprint.points.at(i).x;
    point.y = shape.footprint.points.at(i).y;
    point.z = shape.dimensions.z / 2.0;
    points.push_back(point);
    point.x = shape.footprint.points.at((int)(i + 1) % (int)shape.footprint.points.size()).x;
    point.y = shape.footprint.points.at((int)(i + 1) % (int)shape.footprint.points.size()).y;
    point.z = shape.dimensions.z / 2.0;
    points.push_back(point);
  }
  for (size_t i = 0; i < shape.footprint.points.size(); ++i) {
    geometry_msgs::Point point;
    point.x = shape.footprint.points.at(i).x;
    point.y = shape.footprint.points.at(i).y;
    point.z = -shape.dimensions.z / 2.0;
    points.push_back(point);
    point.x = shape.footprint.points.at((int)(i + 1) % (int)shape.footprint.points.size()).x;
    point.y = shape.footprint.points.at((int)(i + 1) % (int)shape.footprint.points.size()).y;
    point.z = -shape.dimensions.z / 2.0;
    points.push_back(point);
  }
  for (size_t i = 0; i < shape.footprint.points.size(); ++i) {
    geometry_msgs::Point point;
    point.x = shape.footprint.points.at(i).x;
    point.y = shape.footprint.points.at(i).y;
    point.z = shape.dimensions.z / 2.0;
    points.push_back(point);
    point.x = shape.footprint.points.at(i).x;
    point.y = shape.footprint.points.at(i).y;
    point.z = -shape.dimensions.z / 2.0;
    points.push_back(point);
  }
  return true;
}

bool DynamicObjectVisualizer::calcPathLineList(
  const autoware_perception_msgs::PredictedPath & paths, std::vector<geometry_msgs::Point> & points)
{
  for (int i = 0; i < (int)paths.path.size() - 1; ++i) {
    geometry_msgs::Point point;
    point.x = paths.path.at(i).pose.pose.position.x;
    point.y = paths.path.at(i).pose.pose.position.y;
    point.z = paths.path.at(i).pose.pose.position.z;
    points.push_back(point);
    point.x = paths.path.at(i + 1).pose.pose.position.x;
    point.y = paths.path.at(i + 1).pose.pose.position.y;
    point.z = paths.path.at(i + 1).pose.pose.position.z;
    points.push_back(point);
    calcCircleLineList(point, 0.5, points, 10);
  }
  return true;
}

void DynamicObjectVisualizer::initPose(geometry_msgs::Pose & pose)
{
  pose.position.x = 0.0;
  pose.position.y = 0.0;
  pose.position.z = 0.0;
  pose.orientation.x = 0.0;
  pose.orientation.y = 0.0;
  pose.orientation.z = 0.0;
  pose.orientation.w = 1.0;
}

bool DynamicObjectVisualizer::getLabel(
  const autoware_perception_msgs::Semantic & semantic, std::string & label)
{
  if (autoware_perception_msgs::Semantic::UNKNOWN == semantic.type) {
    label = std::string("unknown");
  } else if (autoware_perception_msgs::Semantic::CAR == semantic.type) {
    label = std::string("car");
  } else if (autoware_perception_msgs::Semantic::TRUCK == semantic.type) {
    label = std::string("truck");
  } else if (autoware_perception_msgs::Semantic::BUS == semantic.type) {
    label = std::string("bus");
  } else if (autoware_perception_msgs::Semantic::BICYCLE == semantic.type) {
    label = std::string("bicycle");
  } else if (autoware_perception_msgs::Semantic::MOTORBIKE == semantic.type) {
    label = std::string("motorbike");
  } else if (autoware_perception_msgs::Semantic::PEDESTRIAN == semantic.type) {
    label = std::string("pedestrian");
  } else if (autoware_perception_msgs::Semantic::ANIMAL == semantic.type) {
    label = std::string("animal");
  } else {
    label = std::string("other");
  }
  return true;
}

void DynamicObjectVisualizer::getColor(
  const autoware_perception_msgs::DynamicObject & object, std_msgs::ColorRGBA & color)
{
  std::string id_str = unique_id::toHexString(object.id);
  std::remove(id_str.begin(), id_str.end(), '-');
  int i = ((int)id_str.at(0) * 4 + (int)id_str.at(1)) % (int)colors_.size();
  color.r = colors_.at(i).r;
  color.g = colors_.at(i).g;
  color.b = colors_.at(i).b;
}

void DynamicObjectVisualizer::initColorList(std::vector<std_msgs::ColorRGBA> & colors)
{
  std_msgs::ColorRGBA sample_color;
  sample_color.r = 1.0;
  sample_color.g = 0.0;
  sample_color.b = 1.0;
  colors.push_back(sample_color);  // magenta
  sample_color.r = 0.69;
  sample_color.g = 1.0;
  sample_color.b = 0.18;
  colors.push_back(sample_color);  // green yellow
  sample_color.r = 0.59;
  sample_color.g = 1.0;
  sample_color.b = 0.59;
  colors.push_back(sample_color);  // pale green
  sample_color.r = 0.5;
  sample_color.g = 1.0;
  sample_color.b = 0.0;
  colors.push_back(sample_color);  // chartreuse green
  sample_color.r = 0.12;
  sample_color.g = 0.56;
  sample_color.b = 1.0;
  colors.push_back(sample_color);  // dodger blue
  sample_color.r = 0.0;
  sample_color.g = 1.0;
  sample_color.b = 1.0;
  colors.push_back(sample_color);  // cyan
  sample_color.r = 0.54;
  sample_color.g = 0.168;
  sample_color.b = 0.886;
  colors.push_back(sample_color);  // blueviolet
  sample_color.r = 0.0;
  sample_color.g = 1.0;
  sample_color.b = 0.5;
  colors.push_back(sample_color);  // spring green
}
